const router = require('express').Router();
const controllers = require('../controllers/ctrl-index');
const controlMarket = require('../controllers/ctrl-market-rates');

router.route('/').get(controllers.home);
router.route('/marketrates').get(controlMarket.loadTables);
router.route('/:exchange/:coin/:fiat/:volumen').get(controllers.getDataV);
router.route('/:exchange/:coin/:fiat').get(controllers.getData);

module.exports = router;