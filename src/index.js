const express = require('express');
const router = require('./routes/routes-view');
const path = require('path');
require('dotenv').config();
const app = express();

//settings
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, './views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');


//routes
app.use(router);


// middleware

//static files
app.use(express.static(path.join(__dirname, 'public')));

//start the server
app.listen(app.get('port'), () => {
    console.log(`Server listening on port ${app.get('port')}`);
});


