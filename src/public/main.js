$(function () {
    var $coin = $('select[name=coin]');
    var $exchange_buy = $('select[name=exchangebuy]');
    var $exchange_sell = $('select[name=exchangesell]');
    var $amount = $('input[name=amount]');

    var $ul1 = $('ul[name=results]');
    var $ul2 = $('ul[name=market_rates]');

    var totalbuy = 0;
    var totalsell = 0;



    $('form').on('submit', function (event) {
        event.preventDefault();

        coin = $coin.val();
        exchangebuy = $exchange_buy.val();
        exchangesell = $exchange_sell.val();
        amount = parseFloat($amount.val());


        $ul1.html(`<p>Solving...</p>`);
        $ul2.html(`<p>Solving...</p>`);

        // ####################### BUY

        var reqbuy = $.ajax({
            url: `/${exchangebuy}/${coin}/ars`,
            data: 'json'
        });

        reqbuy.done(function (data) {
            totalbuy = parseFloat(data.totalAsk);
            
            // ####################### SELL

            var reqsell = $.ajax({
                url: `/${exchangesell}/${coin}/ars`,
                data: 'json'
            });

            reqsell.done(function (data) {
                totalsell = parseFloat(data.totalBid);
                
                // ####################### SHOW RESULTS

                var amount_coins = amount / totalbuy;
                var profit = totalsell * amount_coins;
                var netprofit = profit - amount;
                var percentage = (netprofit * 100) / profit;


                $ul1.html(`<li><p>Amount entered: $ ${amount.toFixed(2)}</p></li>
                   <li><p>Profit: $ ${profit.toFixed(2)}</p></li>
                   <li><p>Net profit: $ ${netprofit.toFixed(2)}</p></li>
                   <li><p>Percentage obtained: ${percentage.toFixed(2)} %</p></li>`);

                $ul2.html(`<li><p class="text-capitalize"><strong>${exchangebuy}:</strong></p></li>
                      <ul><li><p>Total buy: $ ${totalbuy.toFixed(2)}</p></li></ul>
                   <li><p class="text-capitalize"><strong>${exchangesell}:</strong></p></li>
                      <ul><li><p>Total sell: $ ${totalsell.toFixed(2)}</p></li></ul>`);

                $amount.val("");
            });

            reqsell.fail(function () {
                $ul1.html('Error!');
            });

        });

        reqbuy.fail(function () {
            $ul1.html('Error!');
        });


    });

});