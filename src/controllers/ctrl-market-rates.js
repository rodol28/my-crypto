const axios = require('axios');

dai = [];
ethereum = [];
bitcoin = [];

module.exports = {

    loadTables: async (req, res) => {

        await axios.get(`https://criptoya.com/api/buenbit/dai/ars`)
            .then(response => {
                dai.push({
                    exchange: 'Buenbit',
                    coin: 'dai',
                    fiat: 'ars',
                    ask: response.data.ask,
                    totalAsk: response.data.totalAsk,
                    bid: response.data.bid,
                    totalBid: response.data.totalBid,
                    time: response.data.time
                });
            })
            .catch(err => {
                console.log(err);
                next();
                return;
            });

        await axios.get(`https://criptoya.com/api/qubit/dai/ars`)
            .then(response => {
                dai.push({
                    exchange: 'Qubit',
                    coin: 'dai',
                    fiat: 'ars',
                    ask: response.data.ask,
                    totalAsk: response.data.totalAsk,
                    bid: response.data.bid,
                    totalBid: response.data.totalBid,
                    time: response.data.time
                });
            })
            .catch(err => {
                console.log(err);
                next();
                return;
            });

        await axios.get(`https://criptoya.com/api/ripio/dai/ars`)
            .then(response => {
                dai.push({
                    exchange: 'Ripio',
                    coin: 'dai',
                    fiat: 'ars',
                    ask: response.data.ask,
                    totalAsk: response.data.totalAsk,
                    bid: response.data.bid,
                    totalBid: response.data.totalBid,
                    time: response.data.time
                });
            })
            .catch(err => {
                console.log(err);
                next();
                return;
            });

        // ################################ ETHEREUM
        await axios.get(`https://criptoya.com/api/qubit/eth/ars`)
            .then(response => {
                ethereum.push({
                    exchange: 'Qubit',
                    coin: 'ethereum',
                    fiat: 'ars',
                    ask: response.data.ask,
                    totalAsk: response.data.totalAsk,
                    bid: response.data.bid,
                    totalBid: response.data.totalBid,
                    time: response.data.time
                });
            })
            .catch(err => {
                console.log(err);
                next();
                return;
            });

        await axios.get(`https://criptoya.com/api/ripio/eth/ars`)
            .then(response => {
                ethereum.push({
                    exchange: 'Ripio',
                    coin: 'ethereum',
                    fiat: 'ars',
                    ask: response.data.ask,
                    totalAsk: response.data.totalAsk,
                    bid: response.data.bid,
                    totalBid: response.data.totalBid,
                    time: response.data.time
                });
            })
            .catch(err => {
                console.log(err);
                next();
                return;
            });

        // ################################ BITCOIN
        await axios.get(`https://criptoya.com/api/buenbit/btc/ars`)
            .then(response => {
                bitcoin.push({
                    exchange: 'Buenbit',
                    coin: 'bitcoin',
                    fiat: 'ars',
                    ask: response.data.ask,
                    totalAsk: response.data.totalAsk,
                    bid: response.data.bid,
                    totalBid: response.data.totalBid,
                    time: response.data.time
                });
            })
            .catch(err => {
                console.log(err);
                next();
                return;
            });

        await axios.get(`https://criptoya.com/api/qubit/btc/ars`)
            .then(response => {
                bitcoin.push({
                    exchange: 'Qubit',
                    coin: 'bitcoin',
                    fiat: 'ars',
                    ask: response.data.ask,
                    totalAsk: response.data.totalAsk,
                    bid: response.data.bid,
                    totalBid: response.data.totalBid,
                    time: response.data.time
                });
            })
            .catch(err => {
                console.log(err);
                next();
                return;
            });

       await axios.get(`https://criptoya.com/api/ripio/btc/ars`)
            .then(response => {
                bitcoin.push({
                    exchange: 'Ripio',
                    coin: 'bitcoin',
                    fiat: 'ars',
                    ask: response.data.ask,
                    totalAsk: response.data.totalAsk,
                    bid: response.data.bid,
                    totalBid: response.data.totalBid,
                    time: response.data.time
                });

                res.render('market-rates.html', {
                    dai: dai,
                    ethereum: ethereum,
                    bitcoin: bitcoin
                });
            })
            .catch(err => {
                console.log(err);
                next();
                return;
            });


    } //end loadTables
};