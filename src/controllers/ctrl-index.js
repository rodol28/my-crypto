const axios = require('axios');

module.exports = {

    home: (req, res) => {
        res.render('view-index.html');
    },

    // RUTA PARA CONSULTAR INDICANDO UN VOLUMEN
    getDataV: (req, res) => {
        if (!req.params.exchange || !req.params.coin || !req.params.fiat || !req.params.volumen) {
            res.status(404).json({
                error: 'Invalid'
            });
        }

        let exchange = req.params.exchange;
        let coin = req.params.coin;
        let fiat = req.params.fiat;
        let volumen = parseFloat(req.params.volumen, 10);

        axios.get(`https://criptoya.com/api/${exchange}/${coin}/${fiat}/${volumen}`)
            .then(response => {

                res.render('view-index.html', {
                    data: {
                        exchange: exchange,
                        coin: coin,
                        fiat: fiat,
                        ask: response.data.ask,
                        totalAsk: response.data.totalAsk,
                        bid: response.data.bid,
                        totalBid: response.data.totalBid,
                        time: response.data.time
                    }
                });
            })
            .catch(err => {
                console.log(err);
                next();
                return;
            });
    },


    // RUTA PARA CONSULTAR SIN INDICANDO UN VOLUMEN
    getData: (req, res) => {
        if (!req.params.exchange || !req.params.coin || !req.params.fiat) {
            res.status(404).json({
                error: 'Invalid'
            });
        }

        let exchange = req.params.exchange;
        let coin = req.params.coin;
        let fiat = req.params.fiat;

        axios.get(`https://criptoya.com/api/${exchange}/${coin}/${fiat}`)
            .then(response => {
                res.json({
                    exchange: exchange,
                    coin: coin,
                    fiat: fiat,
                    ask: response.data.ask,
                    totalAsk: response.data.totalAsk,
                    bid: response.data.bid,
                    totalBid: response.data.totalBid,
                    time: response.data.time
                });
            })
            .catch(err => {
                console.log(err);
                next();
                return;
            });
    }
};